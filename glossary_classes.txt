﻿Rank 1 	

Archer — Лучник
Cleric — Жрец
Swordsman — Мечник
Wizard — Волшебник

Rank 2 	

Ranger — Рэйнджер
Quarrelshooter — Арбалетчик
Krivis — Криве
Priest — Священник
Highlander — Горец
Peltasta — Пельтаст
Pyromancer — Пиромант
Cryomancer — Криомант

Rank 3
 	
Sapper — Сапёр (?)
Hunter — Охотник
Bokor — Бокор
Dievdirbys — Дэвдирбис
Hoplite — Гоплит
Barbarian — Варвар
Psychokino — Психокинетик
Linker — Медиум

Rank 4 	

Wugushi — Вугуши
Scout — Разведчик(?)
Sadhu — Садху
Paladin — Паладин
Rodelero — Родельеро
Cataphract — Катафрактарий
Thaumaturge — Тауматург
Elementalist — Элементалист

Rank 5 	

Rogue — Разбойник
Fletcher — Стрельник
Monk — Монах
Pardoner — Продавец индульгенций (?)
[hidden]Chaplain — Капеллан
Squire — Сквайр
Corsair — Корсар
Sorcerer — Чародей (?)
Chronomancer  — Хрономант

Rank 6 
	
Falconer — (?) Сокольничий
Schwarzer Reiter — (?) Шварце Райтер
Oracle — Оракул
Druid — Друид
Doppelsoeldner — Доппельзольднер
Fencer — (?) Фехтовальщик
Necromancer — Некромант
Alchemist — Алхимик
[hidden]Rune Caster — Заклинатель рун

Rank 7
 	
Cannoneer — (?)Канонир/Пушкарь
Musketeer — Мушкетёр
Plague Doctor — Чумной доктор
Kabbalist — Каббалист
Dragoon — Драгун
Templar — (?)Храмовник/Тамплиер
Warlock — (?)Колдун
Featherfoot — Кадиш (?Курдайча/Кадайча)
[hidden]Shinobi — Шиноби

Rank ?
 	
Pied Piper — Крысолов (?)
Appraiser — Оценщик (?)
Shepherd — Пастырь
Centurion — Центурион
Mimic — Мимик (?) / Мим
Taoist Priest — (?)Таоист
Hackapell — Хаккапелита
Murmillo — Мурмиллон
Sage  — (?)Мудрец


